import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
    {
      path: '/',
      name: 'Home',
      component: () => import('../views/Home.vue')
    },
    {
      path: '/education',
      name: 'Education',
      component: () => import('../views/Education.vue')
    },
    {
      path: '/laws',
      name: 'Laws',
      component: () => import('../views/Laws.vue')
    },
    {
      path: '/signin',
      name: 'Signin',
      component: () => import('../views/Signin.vue')
    },
    {
      path: '/signup',
      name: 'Signup',
      component: () => import('../views/Signup.vue')
    },
    {
      path: '/logout',
      name: 'Logout',
      component: () => import('../views/Logout.vue')
    },
      {
      path: '/articles/:id',
      component: () => import('../views/ArticlePage.vue')
    },
    {
      path: '/exams/:id',
      component: () => import('../views/Exam.vue')
    },
    {
      path: '/tests/:id',
      component: () => import('../views/Test.vue')
    },
    {
      path: '/temp',
      name: 'Temp',
      component: () => import('../views/Temp.vue')
    },
    {
      path: '*',
      name: 'PageNotFound',
      component: () => import('../views/PageNotFound.vue')
    }
  ]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
